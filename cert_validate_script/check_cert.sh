#!/bin/bash

#=============================================================================
#          FILE:  check_certs.sh
#         USAGE:  ./check_certs.sh -p <PARTNER>
#   DESCRIPTION:
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  salmen HITANA, salmen.hitana@sofrecom.com
#       COMPANY:  Sofrecom
#       VERSION:  1.0
#       CREATED:  10/01/2013 10:31:01 PM MDT
#      REVISION:  ---
#===============================================================================

STATUS_OK=0
STATUS_UNKNOWN=1
STATUS_NO_PARTNER=2
STATUS_WARNING=3
STATUS_CRITICAL=4
STATUS_NO_CERTSTATUS_NO_CERT=5

TODAY=$(date +%s)

#===============================================================================
#         USAGE: usage
#   DESCRIPTION: Display help
#===============================================================================
usage()
{
cat << EOF
NOK - Usage: $(basename $0) -p <partner>

OPTIONS:
        -h      Show this message
        -H      IP ou DNS to check
	-p 	Port number
Example : ./$(basename $0) -H localhost -p 443
EOF
}

[ $# -lt 2 ] && usage && exit  $STATUS_UNKNOWN
while getopts "h:H:p:" OPTION
do
	case $OPTION in
	h)
		usage
		exit $STATUS_UNKNOWN
		;;
        H)
                HOSTNAME=$OPTARG
                ;;	
	p)
		PORT=$OPTARG
		## Add valid port number verification
		#if [[ $(echo $PARTNER| wc -c) -lt 3 ]]
		#then
			#now=$(date +"%Y/%m/%d %H:%M:%S")
			#msg="$now - ERROR - Please enter a valid port number"
			#echo $msg | tee -a /tmp/check_cert.log
			#exit $STATUS_NO_VALID_PORT
		#fi
		;;
	?)
		usage
		exit $STATUS_UNKNOWN
		;;
	esac
done


#===============================================================================
#         USAGE: mysdate <DATE>
#   DESCRIPTION: Format experation date of certificate
#===============================================================================
function mydate()
{
	local y=$( echo $1 | cut -d" " -f4 )
	local M=$( echo $1 | cut -d" " -f1 )
	local d=$( echo $1 | cut -d" " -f2 )
	local m
	if [ ${d} -lt 10 ]; then
		d="0${d}";
	fi
	case $M in
			Jan) m="01";;
			Feb) m="02";;
			Mar) m="03";;
			Apr) m="04";;
			May) m="05";;
			Jun) m="06";;
			Jul) m="07";;
			Aug) m="08";;
			Sep) m="09";;
			Oct) m="10";;
			Nov) m="11";;
			Dec) m="12";;
	esac
	echo "${m}/${d}/${y}"
}

#===============================================================================
#         USAGE: display [table]
#   DESCRIPTION: Display table
#===============================================================================
function display ()
{
	echo "function: DISPLAY: $1"
	local tab1=($(eval echo $(echo \${$1[@]})))
	for (( i=0; i<${#tab1[*]}; i=i+1))
	do
			echo ${tab1[$i]}
	done
	echo "End Function"
}

#===============================================================================
#         USAGE: time_left date1 date2
#   DESCRIPTION: calculate difference between date1 and date2
#===============================================================================
function time_left ()
{
	local cert_date=$(date -d "$1 00:00" +%s)
	local today=$2

	DIFF=$(( $cert_date - $today ))
	echo $(( DIFF / (3600*24) ))/$(( DIFF % (3600*24) / 3600 )):$(( DIFF % 3600 / 60 )):$(( DIFF % 60 ))
}

notafter=$( echo | openssl s_client -connect $HOSTNAME:$PORT 2>/tmp/check_cert.log | openssl x509 -noout -enddate | grep notAfter)
date=$( echo ${notafter} |  sed 's/^notAfter=//' )
CERTDATE=$(mydate "$date")
TIME_LEFT=$(time_left $CERTDATE $TODAY)
DAY_LEFT=$(echo $TIME_LEFT | awk -F'/' '{ print $1 }')
if [ $DAY_LEFT -lt 7 ]
then
	now=$(date +"%Y/%m/%d %H:%M:%S")
	msg="$now - CRITICAL - The certificate will expire in less than one week, expiration date $CERTDATE, days left $DAY_LEFT | ex=4"
	echo $msg | tee -a /tmp/check_cert.log
	statu=$STATUS_CRITICAL
else if [ $DAY_LEFT -lt 31 ]
then 
	now=$(date +"%Y/%m/%d %H:%M:%S")
	msg="$now - WARNING - The certiticate will expire in less than one mounth, expiration date $CERTDATE, days left $DAY_LEFT | ex=3"
	echo $msg | tee -a /tmp/check_cert.log
	statu=$STATUS_WARNING
else
	now=$(date +"%Y/%m/%d %H:%M:%S")
        msg="$now - INFO - The certiticate will expire in more than one mounth, expiration date $CERTDATE, days left $DAY_LEFT | ex=0"
        echo $msg | tee -a /tmp/check_cert.log
	statu=$STATUS_OK
	exit $statu
fi
fi
